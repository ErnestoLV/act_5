#include "auto.h"
#include <iostream>
using namespace std;

Automovil::Automovil(){
  marca = "Bugatti";
  modelo = "Veyron";
  anio = "2011";
  kilometraje = 10000.0;
  numPuertas = "2";
  color = "Negro";
  numPasajeros = 2;
  capacidadTanque = 100;
  gasolinaEnTanque = 16.2;
}

string Automovil :: getMarca(){
  return marca;
}

string Automovil :: getModelo(){
  return modelo;
}

string Automovil :: getAnio(){
  return anio;
}

float Automovil :: getKilometraje(){
  return kilometraje;
}

string Automovil :: getNumPuertas(){
  return numPuertas;
}

float Automovil :: getRendimiento(){
  return rendimiento;
}

float Automovil :: getCapacidadTanque(){
  return capacidadTanque;
}

float Automovil :: getGasolinaEnTanque(){
  return gasolinaEnTanque;
}

void Automovil :: setMarca(string marcaR){
  marca = marcaR;
}

void Automovil :: setModelo(string modeloR){
  modelo = modeloR;
}

void Automovil :: setAnio(string anioR){
  anio = anioR;
}

void Automovil :: setKilometraje(float kilometrajeR){
  kilometraje = kilometrajeR;
}

void Automovil :: setNumPuertas(string numPuertasR){
  numPuertas = numPuertasR;
}

void Automovil :: setColor(string colorR){
  color = colorR;
}

void Automovil :: setNumPasajeros(int numPasajerosR){
  numPasajeros = numPasajerosR;
}

void Automovil :: setRendimiento(float rendimientoR){
  rendimiento = rendimientoR;
}

void Automovil :: setCapacidadTanque(float capacidadTanqueR){
  capacidadTanque = capacidadTanqueR;
}

void Automovil :: setGasolinaEnTanque(float gasolinaEnTanque){
  gasolinaEnTanque = gasolinaEnTanqueR;
}

float Automovil :: validaGasolinaEnTanque(){
  cout << "ingresa cuanta gasolina hay en el tanque: " << endl;
  cin >> gasolinaEnTanque;
  while (gasolinaEnTanque > capacidadTanque | gasolinaEnTanque < 0){
    cout << "Valor no valido, ingrese de nuevo la cantidad: "
    cin >> gasolinaEnTanque;
  }
  return gasolinaEnTanque;
}

float Automovil :: calculaConsumo(float distancia, int pasajeros){
  float consumoCalculado = 0;
  if (pasajeros == numPasajeros)
    consumoCalculado = distancia / rendimiento * 1.05;
  else if (pasajeros < (numPasajeros / 2))
    consumoCalculado = distancia / rendimiento * 0.96;
  else
    consumoCalculado = distancia / rendimiento;

  if(consumoCalculado > gasolinaEnTanque){
    consumoCalculado = -1.0;
  }
  return consumoCalculado;
}

bool Automovil :: requiereMantenimiento(){
  if (kilometraje >= 10000){
    cout << "Mantenimiento requerido!" << endl;
  }
  else if (kilometraje < 10000)
    return false;
}

void Automovil :: avanzaAutomovil(float distancia, int pasajeros){
  float consumoCalculado = calculaConsumo(distancia, pasajeros);
  bool mantenimiento = requiereMantenimiento();
  if(consumoCalculado >= 0 && mantenimiento != true){
    cout << "Avance Confirmado" << endl;
    kilometraje += distancia;
    gasolinaEnTanque -= consumoCalculado;
  }
  else{
    if(consumoCalculado < 0){
      cout << "No hay suficiente gasolina" << endl;
    }
  }

}

void Automovil :: imprimeAutomovil(){
  cout << "Datos del automovil: " << endl;
  cout << "Marca: " << marca << endl;
  cout << "Modelo: " << modelo << endl;
  cout << "A�o: " << anio << endl;
  cout << "Kilometraje: " << kilometraje << endl;
  cout << "Numero de puertas: " << numPuertas << endl;
  cout << "Color: " << color << endl;
  cout << "Capacidad de pasajeros: " << numPasajeros << endl;
  cout << "Rendimiento: " << rendimiento << endl;
  cout << "Capacidad del tanque: " << capacidadTanque << endl;
  cout << "Gasolina en tanque: " << gasolinaEnTanque << endl << endl;

}

void Automovil :: llenaTanque(){
  cout << endl << "Se necesitan " << capacidadTanque - gasolinaEnTanque << "litros para llenar el tanque" << endl;
  gasolinaEnTanque = capacidadTanque;
  cout << "Tanque lleno " << endl;
}
