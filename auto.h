#ifndef AUTO_H_INCLUDED
#define AUTO_H_INCLUDED
#include <string>
using namespace std;
class Automovil{

private:
    string marca;
    string modelo;
    string anio;
    string kilometraje;
    string numPuertas;
    string color;
    string rendimientos;
    string capacidadTanque;
    string gasolinaEnTanque;
public:
    string getMarca();
    string getModelo();
    string getAnio();
    string getKilometraje();
    string getNumPuertas();
    string getRendimiento();
    string getCapacidadTanque();
    string getGasolinaEnTanque();
    void setMarca(marca);
    void setModelo(modelo);
    void setAnio(anio);
    void setKilometraje(kilometraje);
    void setNumPuertas(numPuertas);
    void setColor(color);
    void setNumPasajeros(numPasajeros);
    void setRendimiento(rendimiento);
    void setCapacidadTanque(capacidadTanque);
    void setGasolinaEnTanque(gasolinaEnTanque);
    void avanzaAutomovil(float,int);
    void calculaConsumo(float,int);
    void llenaTanque();
    void imprimeAutomovil();
    void requiereMantenimiento();
    void validaGasolinaEnTanque();

};

#endif // AUTO_H_INCLUDED
